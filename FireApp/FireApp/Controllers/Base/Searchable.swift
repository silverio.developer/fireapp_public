//
//  Searchable.swift
//  FireApp
//
//  Created by Rodrigo on 1/2/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import UIKit
protocol Searchable:class {
    var tableViewBottomConstraint:NSLayoutConstraint! { get set }
}
