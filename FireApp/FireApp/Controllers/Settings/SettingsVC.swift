//
//  SettingsVC.swift
//  FireApp
//
//  Created by Rodrigo on 11/16/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import UIKit

class SettingsVC: BaseTableVC {

    var user: User!


    override func viewDidLoad() {
        super.viewDidLoad()

        user = RealmHelper.getInstance(appRealm).getUser(uid: FireManager.getUid())

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.title = Strings.settings
    }
    
    

    //fix for a tint color bug on IOS 12 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        let image = cell.imageView?.image?.withRenderingMode(.alwaysTemplate)
        if let image = image {
            cell.imageView?.image = image
            cell.imageView?.tintColor = .systemBlue
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "toProfile", sender: nil)
        case 1:
            performSegue(withIdentifier: "toNotifications", sender: nil)
        case 2:
            performSegue(withIdentifier: "toChatSettings", sender: nil)

            case 4:
             if let url = URL(string: Config.privacyPolicyLink) {
                       UIApplication.shared.open(url)
                   }
            
        case 5:
            performSegue(withIdentifier: "toAboutSettings", sender: nil)

        default:
            break
        }
    }

}
