//
//  CallingVC.swift
//  FireApp
//
//  Created by Rodrigo on 11/9/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import UIKit
import Sinch
import RxSwift

class CallingVC: BaseVC {

    let enabledColor = "#a2d5fa".toUIColor()

    @IBOutlet weak var remoteView: UIView!
    @IBOutlet weak var localView: UIView!

    @IBOutlet weak var bottomButtonsStack: UIStackView!
    @IBOutlet weak var btnFlipCamera: UIButton!
    @IBOutlet weak var btnSpeaker: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnMic: UIButton!

    @IBOutlet weak var btnHangup: UIButton!



    @IBOutlet weak var callTypeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var callStateLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!

    @IBOutlet weak var topViewContainer: UIView!
    @IBOutlet weak var bottomViewContainer: UIView!

    @IBOutlet weak var topViewTopCnstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewBottomCnstraint: NSLayoutConstraint!

    private var user: User?
    private var uid: String = ""
    private var phoneNumber: String = ""
    private var conferenceId: String = ""
    private var callType: CallType = .OUTGOING
    private var isVideo = false

    private var isLocalVideoEnabled: Bool = false {
        didSet {
            updateUI()
            let backgroundColor = isLocalVideoEnabled ? enabledColor : .clear
            btnVideo.backgroundColor = backgroundColor

        }
    }

    private var isRemoteViewEnabled: Bool = false {
        didSet {
//            topLayoutSpacing = !isRemoteViewEnabled ? 0 : -topViewContainer.frame.height

            hideOrShowTopViewContainer(hide: isRemoteViewEnabled)
            updateUI()
        }
    }

    private var isFrontCamera: Bool = true {
        willSet {
            if isFrontCamera {
                callManager.client.videoController()?.captureDevicePosition = .back
            } else {
                callManager.client.videoController()?.captureDevicePosition = .front
            }
        }
    }

    private var topLayoutSpacing: CGFloat = 0 {
        didSet {
//            topViewTopCnstraint.constant = topLayoutSpacing
//            UIView.animate(withDuration: 0.2) {
//                self.view.layoutIfNeeded()
//            }

            hideOrShowTopViewContainer(hide: isRemoteViewEnabled)
        }

    }

    private var bottomLayoutSpacing: CGFloat = 0 {
        didSet {

            bottomViewBottomCnstraint.constant = bottomLayoutSpacing

            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }


    private var timerDisposable: Disposable?

    private var callManager: CallManager!
    private var proivderDelegate: ProviderDelegate!

    private var currentCall: SINCall?

    private var isMuted: Bool = false {
        didSet {
            if let call = currentCall {
                proivderDelegate.setMute(call: call, mute: isMuted)
                let backgroundColor = isMuted ? enabledColor : .clear
                btnMic.backgroundColor = backgroundColor
            }
        }
    }

    private var isSpeakerEnabled: Bool = false {
        didSet {
            if let call = currentCall {
                let backgroundColor = isSpeakerEnabled ? enabledColor : .clear
                btnSpeaker.backgroundColor = backgroundColor

                proivderDelegate.setSpeaker(call: call, loud: isSpeakerEnabled)



            }
        }
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //resume timer if the call was established
        if let currentCall = currentCall, currentCall.state == .established {
            scheduleTimer()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timerDisposable?.dispose()
    }

    override func viewDidLoad() {
        super.viewDidLoad()


        callManager = CallManager.sharedInstance
        proivderDelegate = ProviderDelegate.sharedInstance
        proivderDelegate.uiDelegate = self

        if isVideo {
            Permissions.requestMicAndVideoPermissions(completion: nil)
        } else {
            Permissions.requestMicrophonePermissions(completion: nil)
        }

        let callTypeStr = isVideo ? Strings.video_call: Strings.voice_call
        callTypeLbl.text = callTypeStr

        if let user = user {
            userNameLbl.text = user.userName
            callStateLbl.text = Strings.connecting
            if user.userLocalPhoto != "" {
                userImg.image = UIImage(contentsOfFile: user.userLocalPhoto)
            } else {
                userImg.image = user.thumbImg.toUIImage()
            }

            if !user.isGroupBool {

                //fetch the remote user's photo
                FireManager.checkAndDownloadUserPhoto(user: user, appRealm: appRealm).subscribe(onSuccess: { (thumb, image) in
                    self.userImg.image = UIImage(contentsOfFile: image)
                }).disposed(by: disposeBag)

            }

            if callType == .OUTGOING {
                if CallManager.sharedInstance.callWithHandle(user.uid) == nil {


                    if !CallManager.sharedInstance.client.isStarted() {
                        CallManager.sharedInstance.client.start()
                        finishVC()
                        return
                    }
                    if conferenceId.isEmpty {
                        currentCall = ProviderDelegate.sharedInstance.startCall(uid: user.uid, phoneNumber: user.phone, isVideo: isVideo)
                    } else {
                        currentCall = ProviderDelegate.sharedInstance.startConferenceCall(conferenceId: conferenceId, groupName: user.userName)

                    }


                } else {
                    navigationController?.popViewController(animated: true)
                }
            }

            isLocalVideoEnabled = isVideo


            btnVideo.isHidden = !isVideo



        } else {
            //if the user is not exists in local database we will set the name as phoneNumber
            userNameLbl.text = phoneNumber
            if let user = user, !user.isGroupBool {


                FireManager.fetchUserByUid(uid: uid, appRealm: appRealm).subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background)).subscribe(onNext: { (user) in
                    //update call object when fetching user
                    RealmHelper.getInstance(appRealm).updateUserObjectForCall(uid: user.uid, callId: self.currentCall?.callId ?? "");


                    self.userNameLbl.text = user.userName

                }).disposed(by: disposeBag)
            }
        }


        btnHangup.addTarget(self, action: #selector(btnHangupTapped), for: .touchUpInside)

        btnMic.addTarget(self, action: #selector(btnMicTapped), for: .touchUpInside)

        btnSpeaker.addTarget(self, action: #selector(btnSpeakerTapped), for: .touchUpInside)

        btnVideo.addTarget(self, action: #selector(btnVideoTapped), for: .touchUpInside)

        btnFlipCamera.addTarget(self, action: #selector(btnFlipCameraTapped), for: .touchUpInside)


        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
        isSpeakerEnabled = isVideo
        isMuted = false
        updateUI()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }

    @objc private func viewTapped() {
        if !isVideo {
            return
        }

        btnHangup.hideOrShow()
        bottomViewContainer.hideOrShow()
    }

    @objc private func btnHangupTapped() {
        currentCall?.hangup()

        finishVC()
    }

    @objc private func btnFlipCameraTapped() {

        isFrontCamera = !isFrontCamera
    }

    @objc private func btnVideoTapped() {
        if isLocalVideoEnabled {


            currentCall?.pauseVideo()
        } else {

            currentCall?.resumeVideo()
        }

        isLocalVideoEnabled = !isLocalVideoEnabled
    }

    @objc private func btnMicTapped() {
        isMuted = !isMuted
    }

    @objc private func btnSpeakerTapped() {
        isSpeakerEnabled = !isSpeakerEnabled
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    func initialize(uid: String, phoneNumber: String, callType: CallType, isVideo: Bool, call: SINCall? = nil) {
        self.uid = uid
        self.phoneNumber = phoneNumber
        self.callType = callType
        self.isVideo = isVideo
        self.user = RealmHelper.getInstance(appRealm).getUser(uid: uid)
        self.currentCall = call
    }

    func initializeConferenceCall(conferenceId: String, call: SINCall? = nil) {
        self.conferenceId = conferenceId
        print("conference id \(conferenceId)")
        self.uid = conferenceId
        self.callType = .OUTGOING
        self.user = RealmHelper.getInstance(appRealm).getUser(uid: uid)
        self.currentCall = call
    }



    private func scheduleTimer() {
        timerDisposable = Observable<Int>.interval(0.5, scheduler: MainScheduler.instance).subscribe(onNext: { (time) in
            if let call = self.currentCall {
                let duration = call.details.duration

                let formattedTime = duration.timeFormat(false)

                self.callStateLbl.text = formattedTime

            } else {

            }
        })

        timerDisposable?.disposed(by: disposeBag)
    }

    private func updateUI() {


        if let call = currentCall {
            callStateLbl.text = getCallStateString(callState: call.state)

            if call.details.isVideoOffered {
                btnSpeaker.isHidden = true
                btnFlipCamera.isHidden = false

                if let localVideoView = callManager.client.videoController()?.localView() {

                    if isLocalVideoEnabled {
                        localView.isHidden = false
                        localVideoView.contentMode = .scaleAspectFill
                        localView.addSubview(localVideoView)
                    } else {
                        localView.isHidden = true
                    }
                }

                if let remoteVideoView = callManager.client.videoController()?.remoteView(), call.state == .established {
                    if isRemoteViewEnabled {
                        userImg.isHidden = true
                        remoteView.isHidden = false
                        remoteVideoView.contentMode = .scaleAspectFill
                        remoteView.addSubview(remoteVideoView)

                    } else {
                        remoteView.isHidden = true
                        userImg.isHidden = false
                    }
                }
            } else {
                btnSpeaker.isHidden = false
                btnFlipCamera.isHidden = true
            }



        }

    }

    private func finishVC() {
        if let navigation = self.navigationController {

            navigation.popViewController(animated: true)
        } else {

            self.dismiss(animated: true, completion: nil)
        }

    }

    private func hideOrShowTopViewContainer(hide: Bool) {
        localView.translatesAutoresizingMaskIntoConstraints = false


        if hide {
            topViewContainer.isHidden = true
            topViewHeight.constant = 0
        } else {
            topViewContainer.isHidden = false
            topViewHeight.constant = 100
            localView.topAnchor.constraint(equalTo: topViewContainer.bottomAnchor).isActive = true
        }



        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }



}


extension CallingVC: SINCallDelegate {

    func callDidEnd(_ call: SINCall!) {
        let endCause = call.details.endCause
        callStateLbl.text = getCallEndCauseString(cause: endCause)
        timerDisposable?.dispose()




        Observable<Int>.timer(2, scheduler: MainScheduler.instance).subscribe(onCompleted: {
            self.finishVC()
        }).disposed(by: disposeBag)
    }

    func callDidProgress(_ call: SINCall!) {


    }

    func callDidEstablish(_ call: SINCall!) {
        scheduleTimer()
        updateUI()


        proivderDelegate.setMute(call: call, mute: isMuted)
        proivderDelegate.setSpeaker(call: call, loud: isSpeakerEnabled)
        if call.details.isVideoOffered {
            isRemoteViewEnabled = true
        }
    }

    func callDidAddVideoTrack(_ call: SINCall!) {



        if call.state == .established {
            isRemoteViewEnabled = true
        } else {
//            topLayoutSpacing = -topViewContainer.frame.height
            hideOrShowTopViewContainer(hide: isRemoteViewEnabled)

        }

    }

    func callDidPauseVideoTrack(_ call: SINCall!) {

        if call.state == .established {
            isRemoteViewEnabled = false


        }
    }
    func callDidResumeVideoTrack(_ call: SINCall!) {


        if (call.details.isVideoOffered && call.state == .established) {
            isRemoteViewEnabled = true
        }

    }
}


extension CallingVC {

    private func getCallEndCauseString(cause: SINCallEndCause) -> String {


        switch (cause) {
        case .denied:
            return Strings.denied

        case .error:
            return Strings.error

        case .canceled:
            return Strings.cancelled

        case .hungUp:
            return Strings.user_hung_up

        case .noAnswer:
            return Strings.no_answer


        default:
            return "";
        }
    }

    private func getCallStateString(callState: SINCallState) -> String {


        switch (callState) {
        case .initiating,
             .progressing:
            return Strings.connecting

        case .ended:
            return Strings.ended


        default:
            return ""
        }
    }

}

