//
//  AppDelegate.swift
//  FireApp
//
//  Created by Rodrigo on 5/17/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import IQKeyboardManagerSwift
import Sinch
import RxSwift
import FirebaseAuth
import FirebaseDatabase
import FirebaseUI
import MapKit
import SwiftEventBus
import FirebaseMessaging



private let fileURL = FileManager.default
    .containerURL(forSecurityApplicationGroupIdentifier: Config.groupName)!
    .appendingPathComponent("default.realm")
private let config = Realm.Configuration(fileURL: fileURL)
let appRealm = try! Realm(configuration: config)



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var currentChatId = ""

    private var application: UIApplication?


    private var messages: Results<Message>!
    private var messagesNotificationToken: NotificationToken?

    private let disposeBag = DisposeBag()
    private var newNotificationsListeners: NewNotificationsListeners!
    var push: SINManagedPush!

    var client: SINClient!
    var window: UIWindow?

    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }


    private var disposables = [Disposable]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.


        self.application = application
        FirebaseApp.configure()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight


        resetApp(application)


        registerNotifications()
        Messaging.messaging().delegate = self

        GADMobileAds.sharedInstance().start(completionHandler: nil)

        initIQKBMgr()

        self.window = UIWindow()

        goToInitialVC()

        //this will be called when the is killed and the user taps on notification
        if FireManager.isLoggedIn, let notificationData = launchOptions?[.remoteNotification] as? NSDictionary, let chatId = notificationData["chatId"] as? String {

            //wait for view to become alive
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                SwiftEventBus.post(EventNames.notificationTapped, sender: chatId)
            }

        }

        UIApplication.shared.setMinimumBackgroundFetchInterval(15 * 60)


        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogin), name: NSNotification.Name(rawValue: "userDidLogin"), object: nil)

        if FireManager.isLoggedIn && UserDefaultsManager.isUserInfoSaved() {
            setPresenceOnDisconnect()
            listenForConnected()
            syncContactsIfNeeded(appRealm: appRealm)
            initSinchClient()
        }



        newNotificationsListeners = NewNotificationsListeners(disposeBag: disposeBag)

        return true
    }

    func setNotificationsListeners() {
        guard FireManager.isLoggedIn else {
            return
        }

        let newMessageListener = newNotificationsListeners.attachNewMessagesListeners().subscribe(onNext: { (event) in
            SwiftEventBus.post(EventNames.newMessageReceived, sender: event)
            let message = event.0

            let messageType = message.typeEnum

            if messageType.isMediaType() && AutoDownloadPossibility.canAutoDownload(type: messageType) {
                RequestManager.request(message: message, callback: nil, appRealm: appRealm)
            }

            if self.application?.applicationState == .active {
                if message.chatId != AppDelegate.shared.currentChatId {
                    FireManager.updateMessageState(messageId: message.messageId, chatId: message.chatId, state: .RECEIVED, appRealm: appRealm).subscribe().disposed(by: self.disposeBag)
                }
            }

            MessageManager.deleteMessage(messageId: message.messageId).subscribe().disposed(by: self.disposeBag)
        })


        disposables.append(newMessageListener)
        newMessageListener.disposed(by: disposeBag)

        let newGroupListener = newNotificationsListeners.attachNewGroupListeners().subscribe(onNext: { (user) in
            GroupManager.subscribeToGroupTopic(groupId: user.uid).subscribe().disposed(by: self.disposeBag)

            SwiftEventBus.post(EventNames.newGroupCreated, sender: user)
            MessageManager.deleteNewGroupEvent(groupId: user.uid).subscribe().disposed(by: self.disposeBag)
        })

        disposables.append(newGroupListener)
        newGroupListener.disposed(by: disposeBag)



        let deletedMessagesListener = newNotificationsListeners.attachDeletedMessageListener().subscribe(onNext: { (message, user) in

            SwiftEventBus.post(EventNames.messageDeleted, sender: (message, user))

            MessageManager.deleteDeletedMessage(messageId: message.messageId).subscribe().disposed(by: self.disposeBag)

        })

        disposables.append(deletedMessagesListener)
        deletedMessagesListener.disposed(by: disposeBag)


        let missedCallListener = newNotificationsListeners.attachMissedCallsNotifications().subscribe(onNext: { (callId, user) in

            SwiftEventBus.post(EventNames.missedCall, sender: user)

            MessageManager.deleteMissedCall(callId: callId).subscribe().disposed(by: self.disposeBag)
        })


        disposables.append(missedCallListener)
        missedCallListener.disposed(by: disposeBag)

        let listenForScheduledMessageChanges = ScheduledMessagesManager.listenForScheduledMessages2().subscribe(onNext: { (messageId,state) in
            ScheduledMessagesManager.saveMessageAfterSchedulingSucceed(messageId: messageId, state: state)
        })
        
        disposables.append(listenForScheduledMessageChanges)
        listenForScheduledMessageChanges.disposed(by: disposeBag)
        
        
        let listenForScheduledMessageValueChanges = ScheduledMessagesManager.listenForScheduledMessages().subscribe(onNext: { (messageId,state) in
              ScheduledMessagesManager.saveMessageAfterSchedulingSucceed(messageId: messageId, state: state)
          })
          
          disposables.append(listenForScheduledMessageValueChanges)
          listenForScheduledMessageValueChanges.disposed(by: disposeBag)
          
        
        


    }

    func goToInitialVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if !UserDefaultsManager.hasAgreedToPolicy() {
            let vc = storyboard.instantiateViewController(withIdentifier: "mainVc")
            self.window?.rootViewController = vc
        }

        else if !FireManager.isLoggedIn {
            let vc = LoginVC()
            self.window?.rootViewController = vc
        } else {
            if !UserDefaultsManager.isUserInfoSaved() {
                let setupUserVc = storyboard.instantiateViewController(withIdentifier: "SetupUserNavVC")
                self.window?.rootViewController = setupUserVc
            } else {
                let rootVC = storyboard.instantiateViewController(withIdentifier: "RootVC")
                self.window?.rootViewController = rootVC
            }


        }

        self.window?.makeKeyAndVisible()
    }

    func syncContactsIfNeeded(appRealm: Realm) {

        if Permissions.isContactsPermissionsGranted() && UserDefaultsManager.needsSyncContacts() && UserDefaultsManager.isUserInfoSaved() {
            ContactsUtil.syncContacts(appRealm: appRealm).subscribe().disposed(by: disposeBag)
        }
    }

    func setPresenceOnDisconnect() {
        if FireManager.isLoggedIn {
            FireConstants.presenceRef.child(FireManager.getUid()).onDisconnectSetValue(ServerValue.timestamp())
        }
    }

    func listenForConnected() {
        if FireManager.isLoggedIn {

            Database.database().reference(withPath: ".info/connected").rx.observeEvent(.value).flatMap { snapshot -> Observable<DatabaseReference> in
                if let connected = snapshot.value as? Bool {
                    if connected {
                        UnProcessedJobs.process(disposeBag: self.disposeBag)
                        return FireManager.setOnlineStatus()
                    } else {
                        return FireManager.setLastSeen()

                    }
                } else {
                    return Observable.empty()
                }
            }.subscribe().disposed(by: disposeBag)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationWillTerminate(_ application: UIApplication) {


        UserDefaultsManager.setAppTerminated(bool: true)
        UserDefaultsManager.setAppInBackground(bool: true)
        messages = nil
        messagesNotificationToken = nil
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if !FireManager.isLoggedIn {
            completionHandler(.noData)
            return
        }
        if application.applicationState == .background {

            if UserDefaultsManager.getCurrentPresenceState() == .online {
                FireManager.setLastSeen().subscribe(onError: { (error) in
                    completionHandler(.failed)
                }, onCompleted: {
                        completionHandler(.newData)
                    }).disposed(by: disposeBag)
            } else {
                completionHandler(.noData)
            }
        } else {
            completionHandler(.noData)
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {


        if FireManager.isLoggedIn {
            FireManager.setLastSeen().subscribe().disposed(by: disposeBag)
            for disposable in disposables {
                disposable.dispose()
            }
            disposables.removeAll()
        }


        UserDefaultsManager.setAppInBackground(bool: true)
        SwiftEventBus.post(EventNames.appStateChangedEvent, sender: UIApplication.State.background)
    }


    func applicationWillEnterForeground(_ application: UIApplication) {
        // If there is one established call, show the callView of the current call when
        // the App is brought to foreground. This is mainly to handle the UI transition
        // when clicking the App icon on the lockscreen CallKit UI.
        guard FireManager.isLoggedIn else {
            return
        }

        if let call = CallManager.sharedInstance.currentCall {

            if var topController = self.window?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }


                // When entering the application via the App button on the CallKit lockscreen,
                // and unlocking the device by PIN code/Touch ID, applicationWillEnterForeground:
                // will be invoked twice, and "top" will be CallViewController already after
                // the first invocation.
                if !(topController is CallingVC) {
                    topController.performSegue(withIdentifier: "toCallingVC", sender: nil)
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        //fix for textView when it gets pushed down while closing the app and re-open it
        IQKeyboardManager.shared.reloadLayoutIfNeeded()
        SwiftEventBus.post(EventNames.appStateChangedEvent, sender: UIApplication.State.active)
        if FireManager.isLoggedIn {

            RealmHelper.getInstance(appRealm).deleteExpiredStatuses()


            FireManager.setOnlineStatus().subscribe().disposed(by: disposeBag)

            UnProcessedJobs.process(disposeBag: disposeBag)

            setNotificationsListeners()
            
            
            
        }
        UserDefaultsManager.setAppInBackground(bool: false)

        UserDefaultsManager.setAppTerminated(bool: false)






    }



    @objc func userDidLogin() {

        initSinchClient()
        registerNotifications()
        //sync contacts for the first time
        if FireManager.isLoggedIn && UserDefaultsManager.isUserInfoSaved() {
            if Permissions.isContactsPermissionsGranted() && RealmHelper.getInstance(appRealm).getUsers().isEmpty {
                ContactsUtil.syncContacts(appRealm: appRealm).subscribe().disposed(by: disposeBag)
            }

            if disposables.isEmpty {
                setNotificationsListeners()
            }
        }


        FCMTokenSaver.saveTokenToFirebase(token: nil).subscribe().disposed(by: disposeBag)
    }

    private func registerNotifications() {

        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        let center = UNUserNotificationCenter.current()

        center.requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in

                center.delegate = self
            }


        )

        application?.registerForRemoteNotifications()
    }

    func initSinchClient() {
        if FireManager.isLoggedIn {
            push = Sinch.managedPush(with: SinchConfig.environment)
            push.delegate = self
            push.setDesiredPushType(SINPushTypeVoIP)



            let sinchClient = CallManager.sharedInstance.client

            self.client = sinchClient

        }
    }

    func handleSinchNotification(payload: [AnyHashable: Any]) {

        if !FireManager.isLoggedIn {
            return
        }

        if client == nil {
            initSinchClient()
        }



        let isTimedOut = SINPushHelper.queryPushNotificationPayload(payload)?.call()?.isTimedOut

        let headers = SINPushHelper.queryPushNotificationPayload(payload)?.call()?.headers




        ProviderDelegate.sharedInstance.headers = headers

        if let result = client.relayRemotePushNotification(payload), result.isCall(), let call = result.call() {
//




            if call.isTimedOut {


                RealmHelper.getInstance(appRealm).setCallType(callId: call.callId, callType: .MISSED)

            }
        }



    }

    func startClientIfNeeded() {
        if !client.isStarted() {
            client.start()
        }
    }


    fileprivate func resetApp(_ application: UIApplication) {
        let userDefaults = UserDefaults.standard

        if !userDefaults.bool(forKey: "hasRunBefore") {
            // Remove Keychain items here

            do {
                application.applicationIconBadgeNumber = 0
                //if the user was logged in before then sign him out
                try FireManager.auth().signOut()

            } catch let error {

            }
            // Update the flag indicator
            userDefaults.set(true, forKey: "hasRunBefore")
        }
    }

    fileprivate func initIQKBMgr() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.touchResignedGestureIgnoreClasses.append(UnResignableKBView.self)
    }


    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        if response.actionIdentifier == UNNotificationDefaultActionIdentifier, let chatId = response.notification.request.content.userInfo["chatId"] as? String {

            SwiftEventBus.post(EventNames.notificationTapped, sender: chatId)

        }
        completionHandler()
    }


    fileprivate func handleGroupEvent(userInfo: [AnyHashable: Any]) {
        if let groupId = userInfo["groupId"] as? String, let eventId = userInfo["eventId"] as? String, let contextStart = userInfo["contextStart"] as? String, let eventTypeStr = userInfo["eventType"] as? String, let contextEnd = userInfo["contextEnd"] as? String {

            let eventTypeInt = Int(eventTypeStr) ?? 0
            let eventType = GroupEventType(rawValue: eventTypeInt) ?? .UNKNOWN
            //if this event was by the admin himself  OR if the event already exists do nothing
            if contextStart != FireManager.number! && RealmHelper.getInstance(appRealm).getMessage(messageId: eventId) == nil {
                let groupEvent = GroupEvent(contextStart: contextStart, type: eventType, contextEnd: contextEnd)

                let pendingGroupJob = PendingGroupJob(groupId: groupId, type: eventType, event: groupEvent)
                RealmHelper.getInstance(appRealm).saveObjectToRealm(object: pendingGroupJob)
                GroupManager.updateGroup(groupId: groupId, groupEvent: groupEvent).subscribe(onCompleted: {
                    RealmHelper.getInstance(appRealm).deletePendingGroupJob(groupId: groupId)

                }).disposed(by: disposeBag)
            }
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //



//        push.application(application, didReceiveRemoteNotification: userInfo)
        if let event = userInfo["event"] as? String {
            if event == "group_event" {
                handleGroupEvent(userInfo: userInfo)
            } else {
                completionHandler(UIBackgroundFetchResult.noData)
            }



        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {

    }

    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {


        let token = deviceToken.reduce("") { $0 + String(format: "%02x", $1) }

//        push.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)

        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }

    func getUser(uid: String, phone: String) -> User {
        if let user = RealmHelper.getInstance(appRealm).getUser(uid: uid) {
            return user
        }

        //save temp user data while fetching all data later
        let user = User()
        user.phone = phone
        user.uid = uid
        user.userName = phone
        user.isStoredInContacts = false

        RealmHelper.getInstance(appRealm).saveObjectToRealm(object: user)

        return user
    }



    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {

            guard let url = userActivity.webpageURL else { return false }
            guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true) else { return false }
            guard let host = components.host else { return false }

            if let pathComponents = components.path {

                let groupLink = pathComponents.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "/", with: "")

                guard groupLink.isNotEmpty else { return false }

                SwiftEventBus.post(EventNames.groupLinkTapped, sender: groupLink)

                return true
            }
        }
        return false
    }


    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {

        if url.absoluteString == Config.shareUrl {

            if let viewControllers = window?.rootViewController?.children {
                for viewController in viewControllers {
                    if let baseVc = viewController as? Base {
                        //remove events to prevent duplicate unRead counts
                        baseVc.unRegisterEvents()
                    }

                }
            }
            //reset chatId
            currentChatId = ""
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            //
            let shareNavVC = storyboard.instantiateViewController(withIdentifier: "shareNavVC")
            self.window?.rootViewController = shareNavVC
            self.window?.makeKeyAndVisible()



            return true

        }
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true) else { return false }

        
        if let scheme = components.scheme, scheme.starts(with: Config.groupVoiceCallLink), let conferenceId = components.host {

            SwiftEventBus.post(EventNames.groupVoiceCallLinkTapped,sender: conferenceId)
//            if let viewControllers = window?.rootViewController?.children {
//                for viewController in viewControllers {
//                    if let baseVc = viewController as? Base {
//                        //remove events to prevent duplicate unRead counts
//                        baseVc.unRegisterEvents()
//                    }
//
//                }
//            }
//            //reset chatId
//            currentChatId = ""
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//            if let callingVC = storyboard.instantiateViewController(withIdentifier: "CallingVC") as? CallingVC {
//                callingVC.initializeConferenceCall(conferenceId: conferenceId)
//                self.window?.rootViewController = callingVC
//                self.window?.makeKeyAndVisible()
//
//            }


            return true

        }



        return false

    }
}

extension AppDelegate: SINClientDelegate {

    func clientDidStart(_ client: SINClient!) {
        if !UserDefaultsManager.isSinchConfigured() {
            UserDefaultsManager.setSinchConfigured(true)
        }

    }

    func clientDidFail(_ client: SINClient!, error: Error!) {
        UserDefaultsManager.setSinchConfigured(false)

    }


}

extension AppDelegate: SINCallClientDelegate {


    func client(_ client: SINCallClient!, willReceiveIncomingCall call: SINCall!) {


        FireManager.isCallCancelled(userId: call.remoteUserId, callId: call.callId).subscribe(onSuccess: { (isCancelled) in



            if isCancelled || RealmHelper.getInstance(appRealm).isCallCancelled(callId: call.callId) {

                return
            }




            ProviderDelegate.sharedInstance.reportIncomingCall(call)



        }) { (error) in

        }.disposed(by: disposeBag)


    }




    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {




        FireManager.isCallCancelled(userId: call.remoteUserId, callId: call.callId).subscribe(onSuccess: { (isCancelled) in



            if isCancelled || RealmHelper.getInstance(appRealm).isCallCancelled(callId: call.callId) {

                return
            }




            ProviderDelegate.sharedInstance.reportIncomingCall(call)



        }) { (error) in

        }.disposed(by: disposeBag)


    }


}

extension AppDelegate: SINManagedPushDelegate {

    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable: Any]!, forType pushType: String!) {
        DispatchQueue.main.async {
            self.handleSinchNotification(payload: payload)
            self.push.didCompleteProcessingPushPayload(payload)
        }

    }


}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {

        if FireManager.isLoggedIn {
            FCMTokenSaver.saveTokenToFirebase(token: fcmToken).subscribe().disposed(by: disposeBag)
        }
    }


}
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //do not show system notifications while the app is active, instead show NotificationView in VC
        completionHandler([])
    }
}
