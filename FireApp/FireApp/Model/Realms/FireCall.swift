//
//  FireCall.swift
//  FireApp
//
//  Created by Rodrigo on 11/7/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
import RealmSwift

class FireCall: Object {

    override static func primaryKey() -> String? {
        return "callId"
    }

    @objc dynamic var callId = ""
    @objc dynamic var user: User?
    @objc dynamic private var callType = 0

    var type: CallType {
        get {
            return CallType(rawValue: callType)!
        }
        set {
            callType = newValue.rawValue
        }
    }

    @objc dynamic var timestamp = 0
    @objc dynamic var duration = 0
    @objc dynamic var phoneNumber = ""
    @objc dynamic var isVideo = false

    convenience init(callId: String, user: User?, callType: CallType, timestamp: Int, duration: Int, phoneNumber: String, isVideo: Bool) {
        self.init()

        self.callId = callId
        self.user = user
        self.type = callType
        self.timestamp = timestamp
        self.duration = duration
        self.phoneNumber = phoneNumber
        self.isVideo = isVideo
    }
}
