//
//  PhoneNumber.swift
//  FireApp
//
//  Created by Rodrigo on 5/20/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
import RealmSwift
class PhoneNumber: Object {
    @objc dynamic var number = ""
    
    convenience init(number:String) {
        self.init()
        self.number = number
    }
}
