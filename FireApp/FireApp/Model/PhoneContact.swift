//
//  PhoneContact.swift
//  FireApp
//
//  Created by Rodrigo on 9/19/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
class PhoneContact {
    var name:String = ""
    var numbers = [String]()
    
    init(name:String,numbers:[String]) {
        self.name = name
        self.numbers = numbers
    }
}
