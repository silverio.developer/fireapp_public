//
//  CallType.swift
//  FireApp
//
//  Created by Rodrigo on 11/7/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
enum CallType: Int {
    case OUTGOING = 1
    case ANSWERED = 2
    case MISSED = 3
    case INCOMING = 4

}
