//
//  UpdateGroupEvent.swift
//  FireApp
//
//  Created by Rodrigo on 12/5/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
class UpdateGroupEvent: NSObject {
    let groupId:String
    init(groupId:String) {
        self.groupId = groupId
    }
}
