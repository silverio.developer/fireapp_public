//
//  LocalizableButton.swift
//  FireApp
//
//  Created by Rodrigo on 1/8/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import UIKit
class LocalizableButton: UIButton,LocalizableView {
    @IBInspectable var translationKey: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let key = translationKey{
            titleLabel?.text = key.localizedStr
        }
    }
}
