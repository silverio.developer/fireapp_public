//
//  RoundedButton.swift
//  FireApp
//
//  Created by Rodrigo on 11/27/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import UIKit
class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
    }
}
