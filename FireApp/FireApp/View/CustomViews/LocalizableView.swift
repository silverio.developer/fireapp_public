//
//  LocalizableView.swift
//  FireApp
//
//  Created by Rodrigo on 1/7/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import UIKit
protocol LocalizableView {
    var translationKey:String? { get set }
    
}
