//
//  CameraResult.swift
//  FireApp
//
//  Created by Rodrigo on 6/29/19.
//  Copyright © 2019 Rodrigo. All rights reserved.
//

import Foundation
import UIKit
protocol CameraResult {
    func imageTaken(image :UIImage?)  
    func videoTaken(videoUrl:URL)
}
