//
//  GroupVoiceCallLink.swift
//  FireApp
//
//  Created by Rodrigo on 4/22/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import Foundation
class GroupVoiceCallLink {

    static func getVoiceGroupLink(groupId: String) -> String {
        return "\(Config.groupVoiceCallLink)://\(groupId)"
    }
}
