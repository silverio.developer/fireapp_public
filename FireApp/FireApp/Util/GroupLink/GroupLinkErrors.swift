//
//  GroupLinkErrors.swift
//  FireApp
//
//  Created by Rodrigo on 3/1/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import Foundation

class AlreadyInGroupError: NSError {}

class UserBannedFromGroupError: NSError {}

class InvalidGroupLinkError: NSError {}

class UnknownError: NSError {}
