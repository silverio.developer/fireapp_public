//
// Created by Rodrigo on 1/1/20.
// Copyright (c) 2020 Rodrigo. All rights reserved.
//

import Foundation
import Sinch

class SinchConfig {

    private static let DEBUG_ENVIRONMENT = "sandbox.sinch.com";
    private static let RELEASE_ENVIRONMENT = "clientapi.sinch.com";
    public static let environment = SINAPSEnvironment.production

    public static func getSinchConfig() -> SINClient {

        let appKey = Config.sinchAppKey
        let appSecret = Config.sinchAppSecret

        let sinchClient = Sinch.client(withApplicationKey: appKey, applicationSecret: appSecret, environmentHost: RELEASE_ENVIRONMENT, userId: FireManager.getUid())!

        sinchClient.setSupportCalling(true)
        sinchClient.enableManagedPushNotifications()
        




        return sinchClient
    }
}
