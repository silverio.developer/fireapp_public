//
//  SINCallKitProvider.swift
//  FireApp
//
//  Created by Rodrigo on 2/26/20.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import UIKit
import Sinch
import CallKit

class SINCallKitProvider: NSObject {
    private func SINGetCallEndedReason(_ cause: SINCallEndCause) -> CXCallEndedReason {
        switch cause {
        case .error:
            return .failed
        case .denied:
            return .remoteEnded
        case .hungUp:
            // This mapping is not really correct, as SINCallEndCauseHungUp is the end case also when the local peer ended the
            // call.
            return .remoteEnded
        case .timeout:
            return .unanswered
        case .canceled:
            return .unanswered
        case .noAnswer:
            return .unanswered
        case .otherDeviceAnswered:
            return .unanswered
        default:
            break
        }
        return .failed
    }


    var sinClient: SINClient?
    var provider: CXProvider?
    var acDelegate: AudioControllerDelegate
    var calls = [UUID: SINCall?]()
    var incomingCallIds: Set<UUID>

    private func UUIDCallId(_ callId: String?) -> UUID? {
        if callId == nil {
            return nil
        }
        return UUID(uuidString: callId ?? "")
    }


    override init() {

        acDelegate = AudioControllerDelegate()
        calls = [:]
        incomingCallIds = []
        let config = CXProviderConfiguration(localizedName: "Sinch")
        config.maximumCallGroups = 1
        config.maximumCallsPerCallGroup = 1
        config.supportsVideo = true

        provider = CXProvider(configuration: config)
        super.init()
        provider?.setDelegate(self, queue: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(callDidEnd(_:)), name: NSNotification.Name.SINCallDidEnd, object: nil)
    }
    func setClient(_ client: SINClient?) {
//        if self.sinClient?.audioController().delegate == acDelegate {
//            self.sinClient?.audioController().delegate = nil
//        }
        self.sinClient = client
        self.sinClient?.audioController().delegate = acDelegate
    }

    func didReceivePush(withPayload payload: [AnyHashable: Any]?) {
        let notification = SINPushHelper.queryPushNotificationPayload(payload)

        if notification?.isCall() != nil {
            let callNotification = notification?.call()
            let callId = UUIDCallId(callNotification?.callId)

            if applicationState == .active {
                addReportedCall(callId)
                return
            }

            if !hasReportedCall(callId) {
                reportNewIncomingCall(withNotification: callNotification)
            }
        }
    }

    func willReceiveIncomingCall(_ call: SINCall?) {
        add(call)
    }
    func reportNewIncomingCall(withNotification notification: SINCallNotificationResult?) {
        assert((provider != nil), "Invalid parameter not satisfying: provider")

        let callId = UUIDCallId(notification?.callId)

        addReportedCall(callId)

        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: notification?.remoteUserId ?? "")
        update.hasVideo = notification?.isVideoOffered ?? false

        if let callId = callId {
            provider?.reportNewIncomingCall(with: callId, update: update) { error in
                if error != nil {
                    // If we get an error here from the OS, it is possibly the callee's phone has "Do
                    // Not Disturb" turned on, check CXErrorCodeIncomingCallError in CXError.h
                    self.hangupCall(with: callId)
                    if let error = error {
                        
                    }
                }
            }
        }
    }

    func addReportedCall(_ callId: UUID?) {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if let callId = callId {
                incomingCallIds.insert(callId)
            }
        }
    }

    func hasReportedCall(_ callId: UUID?) -> Bool {
        if callId == nil {
            return false
        }


        if let callId = callId {
            return incomingCallIds.contains(callId)
        }
        return false

    }

    func add(_ call: SINCall?) {
        guard let call = call, let uuidCallid = UUIDCallId(call.callId) else {
            return
        }


        calls[uuidCallid] = call
    }

    func removeCall(with callId: UUID?) {
        if let callId = callId {
            calls.removeValue(forKey: callId)
        }


    }

    func call(with callId: UUID?) -> SINCall? {
        if let callId = callId {
            return calls[callId] as? SINCall
        }
        return nil

    }

    func callExists(_ callId: Any?) -> Bool {

        var callIdAsString: String? = nil

        if let uuid = callId as? UUID {
            callIdAsString = uuid.uuidString
        } else if let callId = callId as? String {
            callIdAsString = callId
        }


        for call in activeCalls() ?? [] {
            if .orderedSame == call?.callId.caseInsensitiveCompare(callIdAsString ?? "") {
                return true
            }
        }
        return false
    }

    func activeCalls() -> [SINCall?]? {

        return calls.values as? [SINCall?]

    }
    func currentEstablishedCall() -> SINCall? {
        let calls = activeCalls()
        if calls?.count ?? 0 > 0 && calls?[0]?.state == SINCallState.established {
            return calls?[0] as? SINCall
        } else {
            return nil
        }
    }

    func hangupCall(with callId: UUID?) {
        call(with: callId)?.hangup()
    }

    // Handle cancel/hangup event initiated by either caller or callee
    @objc func callDidEnd(_ notification: Notification?) {
        guard let call = notification?.userInfo?[SINCallKey] as? SINCall else {
            return
        }

        if let uuidCallId = UUIDCallId(call.callId) {
            provider?.reportCall(with: uuidCallId, endedAt: call.details.endedTime, reason: SINGetCallEndedReason(call.details.endCause))
        }

        if callExists(call.callId) {

            removeCall(with: UUIDCallId(call.callId))
        }
    }

    var applicationState: UIApplication.State {
        // Unsafe way of aquiring UIApplicationState on non-main thread / GCD queue
        // without triggering Main Thread Checker.
//        if Thread.isMainThread {
//            return UIApplication.shared.applicationState
//        } else {
//            var state: UIApplication.State
//            DispatchQueue.main.sync(execute: {
//                state = UIApplication.shared.applicationState
//            })
//            return state
//        }
        return UIApplication.shared.applicationState
    }


}

extension SINCallKitProvider: CXProviderDelegate {

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {


        self.sinClient?.call().provider(provider, didActivate: audioSession)
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        self.sinClient?.call().provider(provider, didDeactivate: audioSession)
    }

    func call(for action: CXCallAction?) -> SINCall? {

        let call = self.call(with: action?.uuid)
        if call == nil {
            if let callUUID = action?.callUUID {
                
            }
        }
        return call
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        call(for: action)?.answer()
        self.sinClient?.audioController().configureAudioSessionForCallKitCall()
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        call(for: action)?.hangup()
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        

        if acDelegate.muted {
            self.sinClient?.audioController().unmute()
        } else {
            self.sinClient?.audioController().mute()
        }

        action.fulfill()
    }

    func providerDidReset(_ provider: CXProvider) {
        
    }
}


