//
//  ProviderDelegate.swift
//  Hotline
//
//  Created by Besher on 2018-05-28.
//  Copyright © 2018 Razeware LLC. All rights reserved.
//

import Foundation
import AVFoundation
import CallKit
import Sinch
import RxSwift

private let sharedProviderDelegate = ProviderDelegate()

class ProviderDelegate: NSObject {
    private let disposeBag = DisposeBag()


    class var sharedInstance: ProviderDelegate {
        return sharedProviderDelegate
    }

    fileprivate let provider: CXProvider?
    fileprivate let callController: CXCallController

    var uiDelegate: SINCallDelegate?
    private var audioSession: AVAudioSession?

    var client: SINClient {
        return CallManager.sharedInstance.client
    }
    var acDelegate: AudioControllerDelegate

    override init() {
        provider = CXProvider(configuration: type(of: self).providerConfiguration)

        // SINCH STUFF
        self.acDelegate = AudioControllerDelegate()
        CallManager.sharedInstance.client.audioController().delegate = acDelegate
        

        callController = CXCallController.init()
        super.init()

        provider?.setDelegate(self, queue: nil)

    }

    static var providerConfiguration: CXProviderConfiguration {
        let providerConfiguration = CXProviderConfiguration(localizedName: Config.appName)
        providerConfiguration.supportsVideo = false
        providerConfiguration.maximumCallGroups = 2
        if #available(iOS 11.0, *) {
            providerConfiguration.includesCallsInRecents = true
        }
        providerConfiguration.supportedHandleTypes = [.phoneNumber, .generic]
        return providerConfiguration
    }

    var headers: [AnyHashable: Any]?

    func reportIncomingCall(_ call: SINCall) {


        guard let callHeaders = headers else{
            return
        }

        let number = callHeaders["phoneNumber"] as? String
        let mTimestamp = callHeaders["timestamp"] as? String

        guard let handle = number, let timestampStr = mTimestamp, let uuid = UUID(uuidString: call.callId) else {
            return
        }




        let timestamp = Int(timestampStr) ?? 0
        let user = RealmHelper.getInstance(appRealm).getUser(uid: call.remoteUserId)

        let fireCall = FireCall(callId: call.callId, user: user, callType: .INCOMING, timestamp: timestamp, duration: 0, phoneNumber: handle, isVideo: call.details.isVideoOffered)

        RealmHelper.getInstance(appRealm).saveObjectToRealm(object: fireCall)



        let update = CXCallUpdate()


        update.remoteHandle = CXHandle(type: .phoneNumber, value: handle)
        
        
        update.hasVideo = call.details.isVideoOffered

        
        provider?.reportNewIncomingCall(with: uuid, update: update) { error in
            
            if error == nil {

                call.delegate = self
                CallManager.sharedInstance.addIncoming(call: call)
            }
        }
    }


    var objPlayer: AVAudioPlayer?

    func playAudioFile() {


        guard let url = Bundle.main.url(forResource: "progress_tone", withExtension: "wav") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)

            // For iOS 11
            objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
            objPlayer?.numberOfLoops = -1


            guard let aPlayer = objPlayer else { return }
            aPlayer.play()

        } catch let error {
        }


    }


}

extension ProviderDelegate: CXProviderDelegate {

    func providerDidReset(_ provider: CXProvider) {
        client.audioController().mute()

        for call in CallManager.sharedInstance.calls {
            call.hangup()
        }

        CallManager.sharedInstance.removeAllCalls()
    }
    
    

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        guard let call = CallManager.sharedInstance.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        call.answer()

        action.fulfill()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CallingVC") as! CallingVC

        let fireCall = RealmHelper.getInstance(appRealm).getFireCall(callId: call.callId)
        let phoneNumber = fireCall?.phoneNumber ?? ""
        let isVideo = call.details.isVideoOffered
        let uid = call.remoteUserId!

        vc.initialize(uid: uid, phoneNumber: phoneNumber, callType: .INCOMING, isVideo: isVideo, call: call)
        AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)


    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        guard let call = CallManager.sharedInstance.callWithUUID(uuid: action.callUUID) else {
            
            action.fail()
            return
        }


        
        call.hangup()
        action.fulfill()

        CallManager.sharedInstance.remove(call: call)
    }

    func provider(_ provider: CXProvider, execute transaction: CXTransaction) -> Bool {

        return false
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {

        self.audioSession = audioSession

        client.call().provider(provider, didActivate: audioSession)
        client.audioController().unmute()
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {

        client.audioController().mute()

    }

    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        if acDelegate.muted {
            client.audioController().unmute()
        } else {
            client.audioController().mute()
        }
        action.fulfill()
    }



    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {

        
        if CallManager.sharedInstance.currentCallStatus != .ended {
            CallManager.sharedInstance.currentCall?.delegate = self
            action.fulfill()
            CallManager.sharedInstance.addCall()
        }


    }

    func reportOutgoingStarted(uuid: UUID) {
        provider?.reportOutgoingCall(with: uuid, startedConnectingAt: nil)
    }

    func reportOutoingConnected(uuid: UUID) {
        provider?.reportOutgoingCall(with: uuid, connectedAt: nil)
    }

}

extension ProviderDelegate: SINCallDelegate {



    func callDidProgress(_ call: SINCall!) {

        
        if call.direction == .outgoing {


            reportOutgoingStarted(uuid: call.uuid)
            playAudioFile()

            let startedTime = call.details.startedTime.currentTimeMillis()

            let phoneNumber = call.headers["phoneNumber"] as? String ?? ""

            let uid = call.remoteUserId!
            let user = RealmHelper.getInstance(appRealm).getUser(uid: uid)

            let fireCall = FireCall(callId: call.callId, user: user, callType: .OUTGOING, timestamp: Int(startedTime), duration: 0, phoneNumber: phoneNumber, isVideo: call.details.isVideoOffered)

            RealmHelper.getInstance(appRealm).saveObjectToRealm(object: fireCall)

            uiDelegate?.callDidProgress?(call)

        }


    }

    func callDidEstablish(_ call: SINCall!) {
        if call.direction == .incoming {
            RealmHelper.getInstance(appRealm).setCallType(callId: call.callId, callType: .ANSWERED)
        }
        reportOutoingConnected(uuid: call.uuid)
        objPlayer?.stop()
        uiDelegate?.callDidEstablish?(call)
    }

    func callDidEnd(_ call: SINCall!) {
        
        end(call: call)
        let callDuration = call.details.establishedTime == nil ? 0 : call.details.duration

        RealmHelper.getInstance(appRealm).updateCallInfoOnCallEnded(callId: call.callId, duration: Int(callDuration));
        objPlayer?.stop()
        uiDelegate?.callDidEnd?(call)
        //if the call was cancelled save it to firebase database , so the other user does not notified about it
        if (call.direction == .outgoing && call.details.startedTime == nil) {
            FireManager.setCallCancelled(userId: call.remoteUserId, callId: call.callId).subscribe().disposed(by: disposeBag)
        }
        
        CallManager.sharedInstance.remove(call: call)
        
        
    }

    func callDidResumeVideoTrack(_ call: SINCall!) {
        uiDelegate?.callDidResumeVideoTrack?(call)
    }
    func callDidPauseVideoTrack(_ call: SINCall!) {
        uiDelegate?.callDidPauseVideoTrack?(call)
    }
    func callDidAddVideoTrack(_ call: SINCall!) {
        uiDelegate?.callDidAddVideoTrack?(call)
    }

   

}

extension ProviderDelegate {


    func end(call: SINCall) {
//        let endCallAction = CXEndCallAction(call: call.uuid)
//        let transaction = CXTransaction(action: endCallAction)
//        requestTransaction(transaction)
        provider?.reportCall(with: call.uuid, endedAt: call.details.endedTime, reason: SINGetCallEndedReason(call.details.endCause))
    }

    private func requestTransaction(_ transaction: CXTransaction) {

        callController.request(transaction, completion: { (error: Error?) in

            if error != nil {
                
                //                self.end(call: self.currentCall!)
            }
        })



    }

    func setHeld(call: SINCall, onHold: Bool) {
        let setHeldCallAction = CXSetHeldCallAction(call: call.uuid, onHold: onHold)
        let transaction = CXTransaction()
        transaction.addAction(setHeldCallAction)

        requestTransaction(transaction)
    }

    func setMute(call: SINCall, mute: Bool) {
//        let setMuteCallAction = CXSetMutedCallAction(call: call.uuid, muted: mute)
//        let transaction = CXTransaction()
//        transaction.addAction(setMuteCallAction)
//
//        requestTransaction(transaction)
        
        if mute {

            client.audioController()?.mute()
        } else {
            client.audioController()?.unmute()
        }

    }

    func setSpeaker(call: SINCall, loud: Bool) {

        if loud {
            client.audioController()?.enableSpeaker()
        } else {
            client.audioController()?.disableSpeaker()
        }


    }

    func startCall(uid: String, phoneNumber: String, isVideo: Bool) -> SINCall? {
        var headers = [String: String]()
        headers["phoneNumber"] = phoneNumber
        headers["timestamp"] = Date().currentTimeMillisStr()

        var call: SINCall?

        if isVideo {
            call = client.call()?.callUserVideo(withId: uid, headers: headers)
        } else {
            call = client.call()?.callUser(withId: uid, headers: headers)
        }

        CallManager.sharedInstance.currentCall = call
        call?.delegate = self


        guard let uuid = UUID(uuidString: CallManager.sharedInstance.currentCall?.callId ?? "") else {
            return nil
        }


        let handle = CXHandle(type: CXHandle.HandleType.phoneNumber, value: phoneNumber)
        let startCallAction = CXStartCallAction.init(call: uuid, handle: handle)
        let transaction = CXTransaction.init()
        transaction.addAction(startCallAction)
        requestTransaction(transaction)
        return call
    }
    
    func startConferenceCall(conferenceId: String,groupName:String) -> SINCall? {
        var headers = [String: String]()
        headers["timestamp"] = Date().currentTimeMillisStr()

        

        let call = client.call()?.callConference(withId: conferenceId, headers: headers)

        CallManager.sharedInstance.currentCall = call
        call?.delegate = self


        guard let uuid = UUID(uuidString: CallManager.sharedInstance.currentCall?.callId ?? "") else {
            return nil
        }


        let handle = CXHandle(type: CXHandle.HandleType.generic, value: groupName)
        let startCallAction = CXStartCallAction.init(call: uuid, handle: handle)
        let transaction = CXTransaction.init()
        transaction.addAction(startCallAction)
        requestTransaction(transaction)
        return call
    }
    
    private func SINGetCallEndedReason(_ cause: SINCallEndCause) -> CXCallEndedReason {
           switch cause {
           case .error:
               return .failed
           case .denied:
               return .remoteEnded
           case .hungUp:
               // This mapping is not really correct, as SINCallEndCauseHungUp is the end case also when the local peer ended the
               // call.
               return .remoteEnded
           case .timeout:
               return .unanswered
           case .canceled:
               return .unanswered
           case .noAnswer:
               return .unanswered
           case .otherDeviceAnswered:
               return .unanswered
           default:
               break
           }
           return .failed
       }
}
