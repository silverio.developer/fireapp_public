FireApp is a Real-Time complete chatting app with support for Video & Voice Calls along with Stories feature . you can share images, audio, video, contact, even sharing your location!.

## Features
- Built using Firebase’s SDKs
- Compatible with FireApp for Android
- Phone Authentication
- Schedule a Message to send it on a specific time (BETA)
- Groups Chatting with ability to Join groups via link
- Stories Feature with Text Stories
- Stickers & Filters for Status
- Reply to Message
- Voice & Video Calls
- Group/Conference Voice Calls
- Tap and Hold to Record and share Voice Message
- Firebase Infrastructure(Database,Auth,Storage,Analytics..)
- Sent/Received/Read indicators with Real-Time updates
- Typing/Recording indicators with Real-Time updates
- Copy/Forward/Share/Delete Messages easily
- Last seen and Online states updates
- Delete Messages Remotely
- Works even if you are offline!
- Search for a particular message or a user
- Mute or Block a User
- Image Sharing
- Video Sharing
- Audio Sharing
- Contact Sharing
- Location Sharing
- Emoji Support
- Admob Ready

![alt Text](screenshot.png "screenshot")


## Requirements:
- Mac OS with Xcode 10 or higher
- Paid Apple Developer Account
- [Agora](https://www.agora.io/en)
- [Firebase (Blaze Plan)](https://firebase.google.com/)
